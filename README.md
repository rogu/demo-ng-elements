# DemoNgElements

## branch 1-native-custom-element
```sh
$ cd demo
$ http-server -o
```
Navigate to `http://localhost:8080`

## branch 2-transform-component-into-a-native-custom-elements
```sh
$ ng add @angular/elements
$ npm start
```
Navigate to `http://localhost:4200`

## branch 3-standalone-custom-element
```sh
$ npm run build
$ npm run package
```
Navigate to `http://localhost:8080`

### Useful
https://github.com/manfredsteyer/ngx-build-plus
