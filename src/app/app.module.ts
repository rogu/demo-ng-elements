import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, DoBootstrap } from '@angular/core';
import { ContactCardComponent } from './contact-card/contact-card.component';
import { createCustomElement } from "@angular/elements";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    ContactCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  entryComponents: [ContactCardComponent]
})
export class AppModule implements DoBootstrap {

  constructor(private injector: Injector) { }

  ngDoBootstrap() {
    const el = createCustomElement(ContactCardComponent, { injector: this.injector });
    customElements.define('custom-contact-card', el);
  }

}
